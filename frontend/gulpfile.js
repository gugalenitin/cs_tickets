'use strict';

/**
 * Required plugins
 */
var gulp        = require('gulp'),
    $           = require('gulp-load-plugins')(),
    del         = require('del'),
    fs          = require('fs'),
    path        = require('path'),
    argv        = require('yargs').argv,
    runSequence = require('run-sequence'),
    _           = require('lodash');
var cachebust   = new require('gulp-cachebust')();

/**
 * Config for gulp tasks
 */
var config          = loadConfig();
config.buildVersion = getConfigBuildVersion();
loadEnvironmentArguments();

/**
 * Clean task
 * remove dist folder before running build task
 */
gulp.task('clean', function (cb) {
  return del([
    config.path.build
  ], cb);
});

/**
 * Gulp tasks for Sass
 */
gulp.task('sass', getTask('sass'));

/**
 *    Gulp task for Bower & Minification
 */
gulp.task('minify-js', ['app-config', 'template-cache', 'translation-cache'], getTask('minify-js'));
gulp.task('minify-css', getTask('minify-css'));
gulp.task('minify-html', ['inject-resources'], getTask('minify-html'));
gulp.task('template-cache', getTask('template-cache'));
gulp.task('translation-cache', getTask('translation-cache'));
/**
 * task to copy assets from app to dist
 */
gulp.task('copy-assets', getTask('copy-assets'));
gulp.task('inject-resources', ['minify-js', 'minify-css'], getTask('inject-resources'));

/**
 * Gulp tasks to configure environments
 */
gulp.task('app-config', getTask('app-config'));

/**
 *    Gulp tasks to run application
 */
gulp.task('connect', getTask('connect'));
gulp.task('open', ['connect'], getTask('open'));

/***
 * Will watch for changes and update dist
 */
gulp.task('watch', getTask('watch'));

/*
 * Unit tests
 * */
gulp.task('test', getTask('unit-tests').bind(this, true));

gulp.task('test:auto', getTask('unit-tests').bind(this, false));

/***
 * Will create zipped packages
 */
gulp.task('package-src', getTask('package-src'));
gulp.task('package-dist', ['build'], getTask('package-dist'));

/***
 * Tasks to build app to dist
 */
gulp.task('build:skiptests', function (callback) {
  return runSequence(
    'clean',
    'copy-assets',
    'sass',
    'minify-html',
    callback
  );
});

/***
 * Tasks to build app to dist
 */
gulp.task('build', ['test', 'build:skiptests']);

gulp.task('default', function (callback) {
  runSequence(
    'build:skiptests',
    'watch',
    'open',
    callback
  );
});

gulp.task('run', function (callback) {
  runSequence(
    'build:skiptests',
    'open',
    callback
  );
});

/**
 * Gulp task to run integration-tests
 */
gulp.task('integration-tests', function (callback) {
  gulp
    .src([])
    .pipe($.angularProtractor({
      'configFile'         : 'protractor.conf.js',
      'debug'              : false,
      'autoStartStopServer': true
    }))
    .on('error', function (e) {
      console.log(e);
      //throw e;
    })
    .on('end', callback);
});

/***
 * Helper functions to load tasks async
 ***/

function getTask(task) {
  return require(config.path.gulpTasks + task)(gulp, $, config, path, cachebust);
}

function generateBuildVersionWithDateTime() {
  var currDate = new Date();
  var yr       = currDate.getFullYear();
  var month    = currDate.getMonth() + 1;
  var day      = currDate.getDate();

  return config.defaultBuildVersion + "-" + yr + "." + month + "." + day;
}

function getConfigBuildVersion() {
  return _.isString(argv.v) ? argv.v : generateBuildVersionWithDateTime();
}

//load config according to environment

function loadEnvironmentArguments() {
  if (_.isNumber(argv.uiport)) {
    config.uiServerPort = argv.uiport;
  }
  if (_.isString(argv.uiHost)) {
    config.uiServerHost = argv.uiHost;
  }
  if (_.isNumber(argv.backendport)) {
    config.backendPort = argv.backendport;
  }
  if (_.isNumber(argv.backendProtocol)) {
    config.backendProtocol = argv.backendProtocol;
  }
  if (_.isString(argv.backendHost)) {
    config.backendHost = argv.backendHost;
  }
  if (_.isNumber(argv.idletime)) {
    config.appConfig.idleTime = Number(argv.idletime);
  }
  if (_.isNumber(argv.idletimeout)) {
    config.appConfig.idleTimeout = Number(argv.idletimeout);
  }
}

function loadConfig() {
  return $.environments.production()
    ? require('./gulp/config/config_prod.json')
    : require('./gulp/config/config_dev.json');
}
