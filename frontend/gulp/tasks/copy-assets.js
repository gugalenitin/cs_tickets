/**
 * This task is used to copy asset folder to dist folder
 */
var merge = require('merge-stream');

module.exports = function (gulp, $, config, path) {
  return function () {
    console.log('Copying assets');
    var app = gulp.src(
      [
        path.join(config.path.assets, '**/*'),
        '!' + (path.join(config.path.assets, '**/*.scss'))
      ])
      .pipe(gulp.dest(path.join(config.path.build, 'assets')));

    var version = gulp.src(path.join(config.path.app, 'version.json'))
      .pipe(gulp.dest(config.path.build));

    var fontAwesome = gulp.src([ path.join(config.path.bowerDir, 'font-awesome/fonts/**.*') ])
      .pipe(gulp.dest(path.join(config.path.build, 'assets/fonts')));

    var uiGrid = gulp.src(
      [ path.join(config.path.bowerDir, 'angular-ui-grid/*.ttf'),
        path.join(config.path.bowerDir, 'angular-ui-grid/*.woff')
      ])
      .pipe(gulp.dest(path.join(config.path.build, 'assets/styles')));

    return merge(app, fontAwesome, uiGrid, version);
  };
};