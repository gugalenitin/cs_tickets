/**
 * This task is used to connect to server
 */
module.exports = function (gulp, $, config, path) {
  var development = $.environments.development;
  if (development()) {
    return function () {
      return gulp.src(path.join(config.path.build, 'index.html'))
        .pipe($.open({
          uri: 'http://' + config.uiServerHost +
          ':' + config.uiServerPort + '/'
        }));
    };
  } else {
    return null;
  }
};