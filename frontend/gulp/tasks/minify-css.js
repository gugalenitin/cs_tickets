/**
 * This gulp task minifies the css and stores it in dist/styles folder
 */

var autoprefixer = require('autoprefixer');
var csso         = require('postcss-csso');
var merge        = require('merge-stream');

module.exports = function (gulp, $, config, path, cachebust) {
  var production = $.environments.production;
  return function () {
    console.log('minifying main css');
    var main = gulp.src(
      [
        path.join(config.path.build, 'assets/styles/**/*.css'),
        '!' + (path.join(config.path.build, 'assets/styles/main.css')),
        '!' + (path.join(config.path.build, 'assets/styles/vendor.css'))
      ])
      .pipe($.concat('main.css'))
      .pipe($.postcss([
        autoprefixer({ browsers: config.supportedBrowsers }),
        csso({ restructure: production() })
      ]))
      .pipe(production(cachebust.resources()))
      .pipe(production(gulp.dest(path.join(config.path.build, 'assets/styles/debug/'))))
      .pipe($.size())
      .pipe(gulp.dest(path.join(config.path.build, 'assets/styles')));

    console.log('minifying vendor css');
    var vendor = gulp.src('./bower_components/**/*.css')
      .pipe($.concat('vendor.css'))
      .pipe($.postcss([
        autoprefixer({ browsers: config.supportedBrowsers }),
        csso({ restructure: production() })
      ]))
      .pipe(production(cachebust.resources()))
      .pipe(production(gulp.dest(path.join(config.path.build, 'assets/styles/debug/'))))
      .pipe($.size())
      .pipe(gulp.dest(path.join(config.path.build, 'assets/styles')));

    return merge(main, vendor);
  };
};