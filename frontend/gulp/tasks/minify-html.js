/**
 * This task minifies all the HTML
 */

module.exports = function (gulp, $, config, path) {
  return function () {
    console.log('minifying html');

    var production = $.environments.production;

    return gulp.src(path.join(config.path.build, 'index.html'))
      .pipe(production($.htmlmin()))
      .pipe(gulp.dest(config.path.build));
  };
};