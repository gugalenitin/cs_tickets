module.exports = function (gulp, $, config, path) {
  return $.environments.development()
    ? function () {
    console.log('starting watch');

    var assetsFiles = path.join(config.path.assets, '**/*');

    var jsFiles = [
      path.join(config.path.app, '**/*.js'),
      '!' + path.join(config.path.app, '**/*spec.js')
    ];

    var scssFiles = [
      path.join(config.path.app, '**/*.scss')
    ];

    var htmlFiles = [
      path.join(config.path.app, 'index.html'),
      path.join(config.path.src, '**/*.tpl.html')
    ];

    gulp.watch(assetsFiles, ['copy-assets']);
    gulp.watch(jsFiles, ['minify-js']);
    gulp.watch(scssFiles, ['sass']);
    gulp.watch(jsFiles, ['minify-css']);
    gulp.watch(htmlFiles, ['minify-html']);
  }
    : null;
};