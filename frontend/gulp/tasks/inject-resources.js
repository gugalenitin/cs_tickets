/**
 * Inject links to minified js and css files
 * from dist js/css file to index.html in dist folder
 * read false helps uglify newly added links ?
 * no compress happens if relative true is omitted and leading / also appears?
 */
module.exports = function (gulp, $, config, path) {
  return function () {
    console.log('inject started');

    return gulp.src(path.join(config.path.app, 'index.html'))
      .pipe($.inject(gulp.src(
        [
          path.join(config.path.build, 'scripts/vendor*.js'),
          path.join(config.path.build, 'scripts/main*.js'),
          path.join(config.path.build, 'assets/styles/vendor*.css'),
          path.join(config.path.build, 'assets/styles/main*.css')
        ], { read: false }), { relative: true, ignorePath: '.'+config.path.build}))
      .pipe(gulp.dest(config.path.build))
      .pipe($.connect.reload());
  };
};