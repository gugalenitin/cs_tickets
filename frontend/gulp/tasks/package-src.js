/**
 * This task is used to create zip package
 */

'use strict';

module.exports = function (gulp, $, config) {
  return function () {
    console.log('zipping source files');

    return gulp.src([config.path.app,
        'gulp',
        'aws.json',
        'bower.json',
        '.eslintrc.json',
        'gulpfile.js',
        'package.json',
        'karma.conf.js',
        'protractor.conf.js'])
      .pipe($.zip(config.sourcePackageName))
      .pipe(gulp.dest(config.path.packages));
  };
};