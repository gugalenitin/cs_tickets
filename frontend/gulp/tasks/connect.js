/**
 * This task is used to connect to server
 */
var proxy      = require('http-proxy-middleware');
module.exports = function (gulp, $, config) {
  var backendUrl = [config.backendProtocol, '://', config.backendHost, ':', config.backendPort].join('');
  return function () {
    console.log('connecting server');
    return $.connect.server({
      port      : config.uiServerPort,
      root      : config.path.build,
      host      : config.uiServerHost,
      fallback  : config.path.build + 'index.html',
      livereload: $.environments.development(),
      middleware: $.environments.development(function () {
        return [
          proxy('/api', {
            target      : backendUrl,
            changeOrigin: true
          })
        ]
      })
    });
  };
};