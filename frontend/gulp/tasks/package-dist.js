/**
 * This task is used to create zip packages
 */
'use strict';
var _ = require('lodash');

module.exports = function (gulp, $, config) {
  return function () {
    console.log('zipping dist package');

    var target = _.template(config.distPackageNameTemplate)({version: config.buildVersion});
    return gulp.src(config.path.build + '*')
      .pipe($.zip(target))
      .pipe(gulp.dest(config.path.packages));
  };
};