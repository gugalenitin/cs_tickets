/**
 * This task is used to cache the translations
 */
module.exports = function (gulp, $, config, path) {
  return function () {
    return gulp.src(path.join(config.path.translations, '*.json'))
      .pipe($.angularTranslate({ module: 'csTicketsApp', standalone: false }))
      .pipe(gulp.dest(path.join(config.path.build, 'temp')));
  };
};