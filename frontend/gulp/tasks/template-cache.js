/**
 * This task is used to cache the template
 */
module.exports = function (gulp, $, config, path) {
  return function () {
    return gulp.src(path.join(config.path.app, '**/*.tpl.html'))
      // .pipe($.angularTemplatecache('templates.js', {
      //   module      : 'csTicketsApp',
      //   standalone  : true,
      //   moduleSystem: 'IIFE'
      // }))
      .pipe(gulp.dest(config.path.build));
  };
};