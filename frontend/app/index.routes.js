(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .config(routerConfig);

  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider, routes) {
    $stateProvider
      .state(routes.main, {
        templateUrl: 'src/components/main/main.tpl.html',
        controller : 'MainCtrl as vm',
        abstract   : true
      })
      .state(routes.tickets, {
        templateUrl: 'src/components/tickets/tickets.tpl.html',
        controller : 'TicketsCtrl as vm',
        url        : '/tickets',
        parent     : routes.main
      })
      .state(routes.login, {
        templateUrl: 'src/components/login/login.tpl.html',
        controller : 'LoginCtrl as vm',
        url        : '/login',
        parent     : routes.main
      })
      .state(routes.users, {
        templateUrl: 'src/components/users/users.tpl.html',
        controller : 'UsersCtrl as vm',
        url        : '/users',
        parent     : routes.main
      })
      .state(routes.logout, {
        controller: 'LogoutCtrl as vm',
        url       : '/logout',
        parent    : routes.main
      });

    $urlRouterProvider.otherwise('/login');

    $locationProvider.html5Mode(true);
  }
})();