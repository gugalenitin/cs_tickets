(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .run(appRun);

  function appRun($urlRouter) {
    init();

    function init() {
      $urlRouter.sync();
      $urlRouter.listen();
    }
  }
})();