(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .config(config);

  function config($translateProvider, configurationProvider) {
    configurationProvider.startUp();
    $translateProvider.useSanitizeValueStrategy(null);
  }

})();
