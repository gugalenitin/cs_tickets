(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .controller('UsersCtrl', UsersCtrl);

  function UsersCtrl($rootScope, $scope, $state, usersService, routes) {

    var vm = this;

    if (!$rootScope.currentUser) {
      $state.go(routes.login);
    }

    vm.gridOptions = {
      enableGridMenu       : true,
      enableCellEditOnFocus: true,
      columnDefs           : [
        { name: 'id', enableCellEdit: false },
        { name: 'name' },
        { name: 'email' },
        { name: 'password' },
        { name: 'role' }
      ]
    };

    init();

    function init() {
      usersService.getUsers()
        .then(function (response) {
          vm.gridOptions.data = response;
          vm.gridOptions.data.push({});
        });
    }

    vm.saveRow = function (rowEntity) {
      if (rowEntity.id === undefined) {
        vm.addRow(rowEntity);
        return;
      }
      var promise = usersService.updateUser({ user: rowEntity });
      vm.gridApi.rowEdit.setSavePromise(rowEntity, promise);
    };

    vm.gridOptions.onRegisterApi = function (gridApi) {
      //set gridApi on scope
      vm.gridApi = gridApi;
      gridApi.rowEdit.on.saveRow($scope, vm.saveRow);
    };

    vm.addRow = function (rowEntity) {
      var promise = usersService.addUser({ user: rowEntity });
      vm.gridApi.rowEdit.setSavePromise(rowEntity, promise);
      vm.gridOptions.data.push({});
    };

  }
})();
