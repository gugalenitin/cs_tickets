(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .constant('usersServiceConfig', {
      usersEndpoint: 'users'
    })
    .factory('usersService', usersService);

  function usersService(httpService, urlTemplates, usersServiceConfig) {
    return {
      getUsers  : getUsers,
      updateUser: updateUser,
      addUser   : addUser
    };

    function getUsers() {
      return httpService.get(usersServiceConfig.usersEndpoint);
    }

    function updateUser(userEntity) {
      return httpService.put(urlTemplates.updateUser({ userId: userEntity.user.id }), userEntity);
    }

    function addUser(userEntity) {
      return httpService.post(usersServiceConfig.usersEndpoint, userEntity);
    }
  }

})();
