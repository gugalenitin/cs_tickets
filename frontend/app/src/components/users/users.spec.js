(function () {
  'use strict';

  describe('users', function () {

    describe('UsersCtrl', function () {
      var $rootScope;
      var $scope;
      var $q;
      var $controller;

      var vm;
      var deferred;

      beforeEach(function () {
        module('csTicketsApp');

        module(function ($provide) {
          $provide.service('usersService', createUsersServiceMock);
        });

        inject(function (_$controller_, _$rootScope_, _$q_) {
          $rootScope  = _$rootScope_;
          $scope      = _$rootScope_.$new();
          $q          = _$q_;
          $controller = _$controller_;
        });

        deferred = $q.defer();
        vm       = createController();
      });

      function createController() {
        $rootScope.currentUser = { name: 'Test' };
        return $controller('UsersCtrl', {
          $scope    : $scope,
          $rootScope: $rootScope
        });
      }

      function createUsersServiceMock() {
        return {
          getUsers: jasmine.createSpy('getUsers').and.returnValue(deferred.promise)
        };
      }

      function resolveData(data) {
        $scope.$apply(function () {
          deferred.resolve(data);
        });
      }

      describe('init', function () {

        it('should create a controller', function () {
          expect(vm).toBeDefined();
          expect(vm.constructor.name).toBe('UsersCtrl');
        });

        it('should have initial data', function () {
          expect(vm.gridOptions).not.toBe(null);
        });

        it('should fetch all users', inject(function ($httpBackend) {
          $httpBackend.whenGET('src/components/main/main.tpl.html').respond({ hello: 'World' });
          $httpBackend.expectGET('src/components/main/main.tpl.html');
          $httpBackend.whenGET('src/components/login/login.tpl.html').respond({ hello: 'World' });
          $httpBackend.expectGET('src/components/login/login.tpl.html');
          $httpBackend.flush();

          var users = [ {
            id   : 1,
            name : 'Test',
            email: 'test@crossover.com',
            role : 'Customer'
          } ];
          resolveData(users);
          users.push({});

          expect(vm.gridOptions.data.length).toBe(users.length);
          expect(vm.gridOptions.data).toEqual(users);
        }));

      });

    });

    describe('usersService', function () {
      var usersService;
      var usersServiceConfig;
      var httpService;
      var urlTemplates;

      beforeEach(
        function () {

          module('csTicketsApp');

          module(function ($provide) {
            $provide.service('httpService', createHttpServiceMock);
            $provide.service('urlTemplates', createUrlTemplatesMock);
          });

          inject(function (_usersService_, _usersServiceConfig_, _httpService_, _urlTemplates_) {
            usersService       = _usersService_;
            usersServiceConfig = _usersServiceConfig_;
            httpService        = _httpService_;
            urlTemplates       = _urlTemplates_;
          });

        });

      function createHttpServiceMock() {
        return {
          get : jasmine.createSpy('get'),
          post: jasmine.createSpy('post'),
          put : jasmine.createSpy('put')
        };
      }

      function createUrlTemplatesMock() {
        return {
          updateUser: jasmine.createSpy('updateUser')
        };
      }

      it('should be defined', function () {
        expect(usersService).toBeDefined();
        expect(usersService.getUsers).toBeDefined();
        expect(usersService.updateUser).toBeDefined();
        expect(usersService.addUser).toBeDefined();
      });

      it('should call httpService.get', function () {
        usersService.getUsers();
        expect(httpService.get).toHaveBeenCalledWith(usersServiceConfig.usersEndpoint);
      });

      it('should call httpService.post', function () {
        var userObj = { name: 'Test' };
        usersService.addUser(userObj);
        expect(httpService.post).toHaveBeenCalledWith(usersServiceConfig.usersEndpoint, userObj);
      });

      it('should call httpService.put', function () {
        var userObj = { user: { id: 1, name: 'Test' } };
        usersService.updateUser(userObj);
        expect(httpService.put).toHaveBeenCalledWith(urlTemplates.update, userObj);
      });

    });
  });
})();