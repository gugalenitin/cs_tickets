(function () {
  'use strict';

  describe('tickets', function () {

    describe('TicketsCtrl', function () {
      var $rootScope;
      var $scope;
      var $q;
      var $controller;

      var vm;
      var deferred;

      beforeEach(function () {
        module('csTicketsApp');

        module(function ($provide) {
          $provide.service('ticketsService', createTicketsServiceMock);
        });

        inject(function (_$controller_, _$rootScope_, _$q_) {
          $rootScope  = _$rootScope_;
          $scope      = _$rootScope_.$new();
          $q          = _$q_;
          $controller = _$controller_;
        });

        deferred = $q.defer();
        vm       = createController();
      });

      function createController() {
        $rootScope.currentUser = { name: 'Test', role: 'Customer' };
        return $controller('TicketsCtrl', {
          $scope    : $scope,
          $rootScope: $rootScope
        });
      }

      function createTicketsServiceMock() {
        return {
          getTickets: jasmine.createSpy('getTickets').and.returnValue(deferred.promise)
        };
      }

      function resolveData(data) {
        $scope.$apply(function () {
          deferred.resolve(data);
        });
      }

      describe('init', function () {

        it('should create a controller', function () {
          expect(vm).toBeDefined();
          expect(vm.constructor.name).toBe('TicketsCtrl');
        });

        it('should have initial data', function () {
          expect(vm.gridOptions).not.toBe(null);
        });

        it('should fetch all Tickets', inject(function ($httpBackend) {
          $httpBackend.whenGET('src/components/main/main.tpl.html').respond({ hello: 'World' });
          $httpBackend.expectGET('src/components/main/main.tpl.html');
          $httpBackend.whenGET('src/components/login/login.tpl.html').respond({ hello: 'World' });
          $httpBackend.expectGET('src/components/login/login.tpl.html');
          $httpBackend.flush();

          var tickets = [ {
            id         : 1,
            name       : 'Test',
            description: 'Test desc',
            creator    : 'Customer'
          } ];
          resolveData(tickets);
          tickets.push({});

          expect(vm.gridOptions.data.length).toBe(tickets.length);
          expect(vm.gridOptions.data).toEqual(tickets);
        }));

      });

    });

    describe('ticketsService', function () {
      var ticketsService;
      var ticketsServiceConfig;
      var httpService;
      var urlTemplates;

      beforeEach(
        function () {

          module('csTicketsApp');

          module(function ($provide) {
            $provide.service('httpService', createHttpServiceMock);
            $provide.service('urlTemplates', createUrlTemplatesMock);
          });

          inject(function (_ticketsService_, _ticketsServiceConfig_, _httpService_, _urlTemplates_) {
            ticketsService       = _ticketsService_;
            ticketsServiceConfig = _ticketsServiceConfig_;
            httpService          = _httpService_;
            urlTemplates         = _urlTemplates_;
          });

        });

      function createHttpServiceMock() {
        return {
          get : jasmine.createSpy('get'),
          post: jasmine.createSpy('post'),
          put : jasmine.createSpy('put')
        };
      }

      function createUrlTemplatesMock() {
        return {
          updateTicket: jasmine.createSpy('updateTicket')
        };
      }

      it('should be defined', function () {
        expect(ticketsService).toBeDefined();
        expect(ticketsService.getTickets).toBeDefined();
        expect(ticketsService.updateTicket).toBeDefined();
        expect(ticketsService.addTicket).toBeDefined();
      });

      it('should call httpService.get', function () {
        ticketsService.getTickets();
        expect(httpService.get).toHaveBeenCalledWith(ticketsServiceConfig.ticketsEndpoint);
      });

      it('should call httpService.post', function () {
        var ticketObj = { name: 'Test' };
        ticketsService.addTicket(ticketObj);
        expect(httpService.post).toHaveBeenCalledWith(ticketsServiceConfig.ticketsEndpoint, ticketObj);
      });

      it('should call httpService.put', function () {
        var ticketObj = { ticket: { id: 1, name: 'Test' } };
        ticketsService.updateTicket(ticketObj);
        expect(httpService.put).toHaveBeenCalledWith(urlTemplates.update, ticketObj);
      });

    });
  });
})();