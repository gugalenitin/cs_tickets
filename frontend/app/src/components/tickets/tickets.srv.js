(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .constant('ticketsServiceConfig', {
      ticketsEndpoint: 'tickets'
    })
    .factory('ticketsService', ticketsService);

  function ticketsService(httpService, urlTemplates, ticketsServiceConfig) {
    return {
      getTickets  : getTickets,
      updateTicket: updateTicket,
      addTicket   : addTicket
    };

    function getTickets() {
      return httpService.get(ticketsServiceConfig.ticketsEndpoint);
    }

    function updateTicket(ticketEntity) {
      return httpService.put(urlTemplates.updateTicket({ ticketId: ticketEntity.ticket.id }), ticketEntity);
    }

    function addTicket(ticketEntity) {
      return httpService.post(ticketsServiceConfig.ticketsEndpoint, ticketEntity);
    }
  }

})();
