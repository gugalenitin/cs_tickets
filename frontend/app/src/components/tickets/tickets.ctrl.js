(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .controller('TicketsCtrl', TicketsCtrl);

  function TicketsCtrl($rootScope, $scope, $state, ticketsService, roles, routes) {

    var vm = this;

    if (!$rootScope.currentUser) {
      $state.go(routes.login);
    }

    vm.gridOptions = {
      enableGridMenu       : true,
      enableCellEditOnFocus: true,
      columnDefs           : [
        { name: 'id', enableCellEdit: false },
        { name: 'name', enableCellEdit: $rootScope.currentUser.role === roles.customer },
        { name: 'description', enableCellEdit: $rootScope.currentUser.role === roles.customer },
        { name: 'status', enableCellEdit: $rootScope.currentUser.role === roles.agent },
        { name: 'creator', enableCellEdit: false }
      ]
    };

    init();

    function init() {
      ticketsService.getTickets()
        .then(function (response) {
          vm.gridOptions.data = response;
          addEmptyRow();
        });
    }

    vm.saveRow = function (rowEntity) {
      if (rowEntity.id === undefined) {
        vm.addRow(rowEntity);
        return;
      }
      var promise = ticketsService.updateTicket({ ticket: rowEntity });
      vm.gridApi.rowEdit.setSavePromise(rowEntity, promise);
    };

    vm.gridOptions.onRegisterApi = function (gridApi) {
      //set gridApi on scope
      vm.gridApi = gridApi;
      gridApi.rowEdit.on.saveRow($scope, vm.saveRow);
    };

    vm.addRow = function (rowEntity) {
      var promise = ticketsService.addTicket({ ticket: rowEntity });
      vm.gridApi.rowEdit.setSavePromise(rowEntity, promise);
      addEmptyRow();
    };

    function addEmptyRow() {
      if ($rootScope.currentUser.role === roles.customer) {
        vm.gridOptions.data.push({});
      }
    }
  }
})();
