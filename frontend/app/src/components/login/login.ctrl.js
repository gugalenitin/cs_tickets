(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .constant('loginConfig', {})
    .controller('LoginCtrl', LoginCtrl);

  function LoginCtrl($rootScope, $state, loginService, roles, routes) {

    var vm   = this;
    vm.login = login;

    vm.username = '';
    vm.password = '';

    checkLogin();

    function checkLogin() {
      if ($rootScope.currentUser) {
        navigateToHomepage();
      }
    }

    function login() {
      loginService.login({
        username: vm.username,
        password: vm.password
      }).then(function (response) {
        $rootScope.currentUser = response;
        localStorage.setItem('currentUser', JSON.stringify(response));
        navigateToHomepage();
      });
    }

    function navigateToHomepage() {
      if ($rootScope.currentUser.role === roles.admin) {
        $state.go(routes.users);
      } else {
        $state.go(routes.tickets);
      }
    }
  }
})();
