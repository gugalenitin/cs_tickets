(function () {
  'use strict';

  describe('login', function () {

    describe('LoginCtrl', function () {
      var $rootScope;
      var $scope;
      var $q;
      var $controller;
  
      var vm;
      var deferred;

      beforeEach(function () {
        module('csTicketsApp');

        module(function ($provide) {
          $provide.service('loginService', createLoginServiceMock);
        });

        inject(function (_$controller_, _$rootScope_, _$q_) {
          $rootScope  = _$rootScope_;
          $scope      = _$rootScope_.$new();
          $q          = _$q_;
          $controller = _$controller_;
        });

        deferred = $q.defer();
        vm       = createController();
      });

      function createController() {
        return $controller('LoginCtrl');
      }

      function createLoginServiceMock() {
        return {
          login: jasmine.createSpy('login').and.returnValue(deferred.promise)
        };
      }

      function resolveData(data) {
        $scope.$apply(function () {
          deferred.resolve(data);
        });
      }

      describe('login', function () {

        it('should create a controller', function () {
          expect(vm).toBeDefined();
          expect(vm.constructor.name).toBe('LoginCtrl');
        });

        it('should have initial data', function () {
          expect(vm.username).toBe('');
          expect(vm.password).toBe('');
        });

        it('should set $rootScope.current user after successful login', inject(function ($httpBackend) {
          $httpBackend.whenGET('src/components/main/main.tpl.html').respond({ hello: 'World' });
          $httpBackend.expectGET('src/components/main/main.tpl.html');
          $httpBackend.whenGET('src/components/login/login.tpl.html').respond({ hello: 'World' });
          $httpBackend.expectGET('src/components/login/login.tpl.html');
          $httpBackend.whenGET('src/components/tickets/tickets.tpl.html').respond({ hello: 'World' });
          $httpBackend.flush();

          expect($rootScope.currentUser).toBe(undefined);
          localStorage.setItem('currentUser', undefined);

          vm.username = 'test';
          vm.password = 'test123';
          vm.login();

          var currentUser = {
            id   : 1,
            name : 'Test',
            email: 'test@crossover.com',
            role : 'Customer'
          };
          resolveData(currentUser);

          expect($rootScope.currentUser).toBe(currentUser);
          expect(JSON.parse(localStorage.getItem('currentUser'))).toEqual(currentUser);
        }));

      });

    });

    describe('loginService', function () {
      var loginService;
      var loginServiceConfig;
      var httpService;

      beforeEach(
        function () {

          module('csTicketsApp');

          module(function ($provide) {
            $provide.service('httpService', createHttpServiceMock);
          });

          inject(function (_loginService_, _loginServiceConfig_, _httpService_) {
            loginService       = _loginService_;
            loginServiceConfig = _loginServiceConfig_;
            httpService        = _httpService_;
          });

        });

      function createHttpServiceMock() {
        return {
          post: jasmine.createSpy('post')
        };
      }

      it('should be defined', function () {
        expect(loginService).toBeDefined();
        expect(loginService.login).toBeDefined();
      });

      it('should call httpService', function () {
        loginService.login({ username: 'test', password: 'test123' });
        expect(httpService.post).toHaveBeenCalledWith(loginServiceConfig.loginEndpoint, {
          username: 'test',
          password: 'test123'
        });
      });

    });
  });
})();