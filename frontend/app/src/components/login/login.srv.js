(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .constant('loginServiceConfig', {
      loginEndpoint: 'login'
    })
    .factory('loginService', loginService);

  function loginService(httpService, loginServiceConfig) {
    return {
      login: login
    };

    function login(loginInfo) {
      return httpService.post(loginServiceConfig.loginEndpoint, loginInfo);
    }

  }

})();
