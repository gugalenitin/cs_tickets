(function () {
  'use strict';

  describe('main', function () {

    describe('MainCtrl', function () {
      var $rootScope;
      var $controller;
      var vm;

      beforeEach(function () {
        module('csTicketsApp');

        inject(function (_$controller_, _$rootScope_) {
          $rootScope  = _$rootScope_;
          $controller = _$controller_;
        });
        localStorage.setItem('currentUser', JSON.stringify({ test: 123 }));
        vm = createController();
      });

      function createController() {
        return $controller('MainCtrl');
      }

      describe('currentUser', function () {

        it('should create a controller', function () {
          expect(vm).toBeDefined();
          expect(vm.constructor.name).toBe('MainCtrl');
        });

        it('should set $rootScope.current user from localStorage', function () {
          expect($rootScope.currentUser).toEqual(JSON.parse(localStorage.getItem('currentUser')));
        });

      });

    });

  });
})();