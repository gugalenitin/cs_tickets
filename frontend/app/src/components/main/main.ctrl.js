(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .controller('MainCtrl', MainCtrl);

  function MainCtrl($rootScope) {
    $rootScope.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }
})();
