(function () {
  'use strict';

  describe('logout', function () {

    describe('LogoutCtrl', function () {
      var $rootScope;
      var $scope;
      var $q;
      var $controller;

      var vm;
      var deferred;

      beforeEach(function () {
        module('csTicketsApp');

        module(function ($provide) {
          $provide.service('logoutService', createLogoutServiceMock);
        });

        inject(function (_$controller_, _$rootScope_, _$q_) {
          $rootScope  = _$rootScope_;
          $scope      = _$rootScope_.$new();
          $q          = _$q_;
          $controller = _$controller_;
        });

        deferred               = $q.defer();
        $rootScope.currentUser = 'Test';
        vm                     = createController();
      });

      function createController() {
        return $controller('LogoutCtrl');
      }

      function createLogoutServiceMock() {
        return {
          logout: jasmine.createSpy('logout').and.returnValue(deferred.promise)
        };
      }

      function resolveData() {
        $scope.$apply(function () {
          deferred.resolve();
        });
      }

      describe('logout', function () {

        it('should create a controller', function () {
          expect(vm).toBeDefined();
          expect(vm.constructor.name).toBe('LogoutCtrl');
        });

        it('should set $rootScope.currentUser to null after successful logout', inject(function ($httpBackend) {
          $httpBackend.whenGET('src/components/main/main.tpl.html').respond({ hello: 'World' });
          $httpBackend.expectGET('src/components/main/main.tpl.html');
          $httpBackend.whenGET('src/components/login/login.tpl.html').respond({ hello: 'World' });
          $httpBackend.expectGET('src/components/login/login.tpl.html');
          // $httpBackend.whenGET('src/components/tickets/tickets.tpl.html').respond({ hello: 'World' });
          $httpBackend.flush();

          resolveData();

          expect($rootScope.currentUser).toBe(null);
          expect(localStorage.getItem('currentUser')).toBe(null);
        }));

      });

    });

    describe('logoutService', function () {
      var logoutService;
      var logoutServiceConfig;
      var httpService;

      beforeEach(
        function () {

          module('csTicketsApp');

          module(function ($provide) {
            $provide.service('httpService', createHttpServiceMock);
          });

          inject(function (_logoutService_, _logoutServiceConfig_, _httpService_) {
            logoutService       = _logoutService_;
            logoutServiceConfig = _logoutServiceConfig_;
            httpService         = _httpService_;
          });

        });

      function createHttpServiceMock() {
        return {
          delete: jasmine.createSpy('delete')
        };
      }

      it('should be defined', function () {
        expect(logoutService).toBeDefined();
        expect(logoutService.logout).toBeDefined();
      });

      it('should call httpService', function () {
        logoutService.logout();
        expect(httpService.delete).toHaveBeenCalledWith(logoutServiceConfig.logoutEndpoint);
      });

    });
  });
})();