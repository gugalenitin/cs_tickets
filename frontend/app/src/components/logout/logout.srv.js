(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .constant('logoutServiceConfig', {
      logoutEndpoint: 'logout'
    })
    .factory('logoutService', logoutService);

  function logoutService(httpService, logoutServiceConfig) {
    return {
      logout: logout
    };

    function logout() {
      return httpService.delete(logoutServiceConfig.logoutEndpoint);
    }

  }

})();
