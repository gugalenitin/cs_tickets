(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .controller('LogoutCtrl', LogoutCtrl);

  function LogoutCtrl($rootScope, $state, logoutService, routes) {

    logout();

    function logout() {
      $rootScope.currentUser = null;
      localStorage.removeItem('currentUser');
      logoutService.logout()
        .then(function () {
          $state.go(routes.login);
        });
    }

  }
})();
