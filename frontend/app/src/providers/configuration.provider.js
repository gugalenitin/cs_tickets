(function () {
  'use strict';

  /**
   * Configuration provider for 'csTicketsApp' module
   * Currently the app does following tasks:
   * i) Load the translations files and set the preferred language of translateProvider
   */
  angular
    .module('csTicketsApp')
    .provider('configuration', configuration);

  function configuration($translateProvider) {
    /***
     * method to load translation files
     * depending upon config file
     */
    function loadTranslations() {
      $translateProvider
        .preferredLanguage('en')
        .useLocalStorage()
        .fallbackLanguage('en');
    }

    /***
     * simple function to configure  the app
     */
    this.startUp = function () {
      loadTranslations();
    };

    this.$get = function () {
    };

  }
})();
