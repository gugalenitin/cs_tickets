(function () {
  'use strict';

  angular.module('csTicketsApp')
    .constant('routes', {
      main   : 'main',
      tickets: 'tickets',
      login  : 'login',
      users  : 'users',
      logout : 'logout'
    });

})();