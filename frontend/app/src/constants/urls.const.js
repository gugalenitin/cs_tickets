(function () {
  'use strict';

  angular.module('csTicketsApp')
    .constant('urls', {
      apiBase: '/api/'
    });

})();