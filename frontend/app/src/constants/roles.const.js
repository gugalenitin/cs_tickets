(function () {
  'use strict';

  angular.module('csTicketsApp')
    .constant('roles', {
      admin   : 'Admin',
      customer: 'Customer',
      agent   : 'Agent'
    });

})();