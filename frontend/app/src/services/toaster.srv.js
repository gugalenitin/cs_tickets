(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .config(function (toastrConfig) {
      angular.extend(toastrConfig, {
        autoDismiss          : false,
        maxOpened            : 5,
        positionClass        : 'toast-top-center',
        preventOpenDuplicates: true,
        timeOut              : 2000
      });
    })
    .constant('toasterConfig', {
      errorTemplate      : '<toaster-error error="error"></toaster-error>',
      errorElem          : 'toaster-error',
      translationKeys    : {
        serverErrorTitle: 'serverErrorTitle'
      },
      defaultErrorOptions: {
        closeButton    : true,
        timeOut        : 5000,
        extendedTimeOut: 5000,
        tapToDismiss   : false,
        allowHtml      : true
      }
    })
    .factory('toaster', toaster);

  function toaster($rootScope, $compile, $window, $translate, _, toastr, toasterConfig) {
    return {
      active   : active,
      clear    : clear,
      error    : error,
      info     : info,
      remove   : remove,
      success  : success,
      warning  : warning,
      httpError: httpError
    };

    function active() {
      return toastr.active();
    }

    function clear(toast) {
      toastr.clear(toast);
    }

    function error(message, title, optionsOverride) {
      return toastr.error(message, title, _.extend({}, toasterConfig.defaultErrorOptions, optionsOverride));
    }

    function info(message, title, optionsOverride) {
      return toastr.info(message, title, optionsOverride);
    }

    function success(message, title, optionsOverride) {
      return toastr.success(message, title, optionsOverride);
    }

    function warning(message, title, optionsOverride) {
      return toastr.warning(message, title, optionsOverride);
    }

    function remove(toastId, wasClicked) {
      toastr.remove(toastId, wasClicked);
    }

    function httpError(errorObj) {
      error(
        toasterConfig.errorTemplate,
        $translate.instant(toasterConfig.translationKeys.serverErrorTitle),
        { onShown: onHttpErrorShown.bind(null, errorObj) }
      );
    }

    function onHttpErrorShown(errorObj) {
      var scope   = $rootScope.$new();
      scope.error = errorObj;
      $compile($window.document.querySelector(toasterConfig.errorElem))(scope);
    }
  }
})();