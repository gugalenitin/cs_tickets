(function () {
  'use strict';

  angular.module('csTicketsApp')
    .factory('urlTemplates', urlTemplates);

  function urlTemplates(_) {
    return {
      updateUser  : _.template('users/<%= userId %>'),
      updateTicket: _.template('tickets/<%= ticketId %>')
    };
  }
})();