(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .factory('currentUserService', currentUserService);

  function currentUserService() {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    return {
      setCurrentUser: setCurrentUser,
      getCurrentUser: getCurrentUser
    };

    function setCurrentUser(user) {
      currentUser = user;
    }

    function getCurrentUser() {
      return currentUser;
    }
  }
})();