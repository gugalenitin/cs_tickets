(function () {
  'use strict';
  
  angular
    .module('csTicketsApp')
    .constant('promiseConfig', {
      promiseCanceled: 'promiseCanceled'
    })
    .factory('promiseService', promiseService);

  function promiseService() {
    var promises = {};

    return {
      cancelAndGetPromise: cancelAndGetPromise
    };

    function cancelAndGetPromise(name, promise) {
      promises[name] && promises[name].cancelLoad && promises[name].cancelLoad();
      promises[name] = null;
      return promises[name] = promise;
    }

  }
})();
