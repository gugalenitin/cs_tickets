(function () {
  'use strict';

  /**
   * This is wrapper over $http and recommended to use it over $http for all
   * communication with Backend API.
   */
  angular
    .module('csTicketsApp')
    .constant('httpServiceConfig', {
      verbs       : {
        GET   : 'GET',
        POST  : 'POST',
        PUT   : 'PUT',
        DELETE: 'DELETE'
      },
      dataKey     : 'data',
      getDataKey  : 'params',
      headers     : { 'Content-Type': 'application/json;charset=utf-8' },
      userEndpoint: 'users/get_user'
    })
    .factory('httpService', httpService);

  function httpService($q, $http, $log, $state, urls, _, toaster, routes, httpServiceConfig, promiseConfig) {
    var verbs = httpServiceConfig.verbs;

    return {
      get   : get,
      post  : post,
      put   : put,
      delete: deleteReq
    };

    function get(relativeUrl, requestData) {
      return makeRequest(prepareOptions(verbs.GET, relativeUrl, requestData));
    }

    function post(relativeUrl, requestData) {
      return makeRequest(prepareOptions(verbs.POST, relativeUrl, requestData));
    }

    function put(relativeUrl, requestData) {
      return makeRequest(prepareOptions(verbs.PUT, relativeUrl, requestData || {}));
    }

    function deleteReq(relativeUrl, requestData) {
      return makeRequest(prepareOptions(verbs.DELETE, relativeUrl, requestData));
    }

    function prepareOptions(verb, relativeUrl, requestData) {
      var url     = urls.apiBase + relativeUrl;
      var options = {
        method         : verb,
        url            : url,
        withCredentials: true
      };
      if (requestData) {
        options[ verb === verbs.GET ? httpServiceConfig.getDataKey : httpServiceConfig.dataKey ] = requestData;
        if (verb === verbs.DELETE) {
          options[ 'headers' ] = httpServiceConfig.headers;
        }
      }
      return options;
    }

    function makeRequest(options) {
      var timeoutPromise = $q.defer();
      var defered        = $q.defer();
      options.timeout    = timeoutPromise.promise;
      $http(options).then(
        function (response) {
          defered.resolve(response.data);
        },
        function (error) {
          if (error.status === 401) {
            toaster.error('Unauthorized!');
            $state.go(routes.login);
            defered.reject(error);
          } else if (error.status === 400) {
            toaster.error('Invalid input!');
            defered.reject(error);
          } else if (error && error.status > 0) {
            if (!(_.isEmpty($state.current.name) && error.config.url.indexOf(httpServiceConfig.userEndpoint) > -1)) {
              toaster.httpError(error);
            }
            $log.error([ 'XHR Error => (status:', error.status, ', url:', error.config.url, ')' ].join(''), error);
            defered.reject(error);
          } else {
            $log.log([ 'XHR Canceled => (url:', error.config.url, ')' ].join(''), error);
            defered.reject(promiseConfig.promiseCanceled);
          }
        });
      defered.promise.cancelLoad = cancel.bind(null, timeoutPromise);
      return ( defered.promise );
    }

    function cancel(timeoutPromise) {
      if (timeoutPromise && timeoutPromise.resolve) {
        timeoutPromise.resolve(promiseConfig.promiseCanceled);
      }
    }
  }
})();