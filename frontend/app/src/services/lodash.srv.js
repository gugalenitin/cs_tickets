(function () {
  'use strict';

  angular
    .module('csTicketsApp')
    .factory('_', lodash);

  function lodash($window) {
    var _ = $window._;
    delete( $window._ );
    return ( _ );
  }
})();