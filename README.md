# Customer Support Ticketing System #
## 1.0 ##

This is a customer support ticketing system app using MySQL, Ruby on Rails for API and AngularJS for UI.


## Requirements ##

* RVM - https://rvm.io/rvm/install
* Ruby - ```rvm install ruby-2.4.0```
* RubyGems - ```rvm rubygems current```
* Ruby on Rails - ```gem install rails```
* MySQL - https://dev.mysql.com/downloads/installer/
* Node.js - https://nodejs.org/en/download/

## MySQL Setup ##

Create user and grant permissions -

```
#!sql

CREATE DATABASE cs_tickets_dev;
CREATE DATABASE cs_tickets_test;
CREATE DATABASE cs_tickets;

CREATE USER 'cs'@'localhost' IDENTIFIED BY 'cs@123';

GRANT ALL ON cs_tickets_dev.* TO 'cs'@'localhost';
GRANT ALL ON cs_tickets_test.* TO 'cs'@'localhost';
GRANT ALL ON cs_tickets.* TO 'cs'@'localhost';

SHOW GRANTS FOR 'cs'@'localhost';
```

## How to run? ##

**Test API -**

```
#!bash

bundle install

rails test
```

**Run API -**

```
#!bash

bundle install

rails db:migrate

rake db:seed

rails s
```  


**Build UI -**
  
  * Install node dependencies - ```npm install```
  * Install bower dependencies - ```bower install```
  
**Test UI -**

```
#!bash

gulp test

```
* If you get gulp not found error then run following command - ```npm install --global gulp-cli```

**Run UI -**
 
```
#!bash

gulp

```