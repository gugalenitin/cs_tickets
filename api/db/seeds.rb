# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

r1 = Role.create({name: "Admin"})
r2 = Role.create({name: "Customer"})
r3 = Role.create({name: "Agent"})

User.create({name: "Admin", email: "admin@crossover.com", password: "admin@123", role: r1})