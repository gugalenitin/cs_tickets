class CreateTickets < ActiveRecord::Migration[5.0]
  def change
    create_table :tickets do |t|
      t.string :name, null: false
      t.text :description, null: false
      t.string :status, null: false
      t.belongs_to :user, foreign_key: true, null: false

      t.timestamps
    end
  end
end
