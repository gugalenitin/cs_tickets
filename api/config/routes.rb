Rails.application.routes.draw do
  scope :api do
    post '/login', to: 'sessions#create'
    delete '/logout', to: 'sessions#destroy'

    post '/users', to: 'users#create'
    get '/users', to: 'users#index'
    put '/users/:id', to: 'users#update'

    post '/tickets', to: 'tickets#create'
    get '/tickets', to: 'tickets#index'
    put '/tickets/:id', to: 'tickets#update'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
