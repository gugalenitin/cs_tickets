class User < ApplicationRecord
  belongs_to :role
  has_secure_password

  def has_role?(role_sym)
    role && role.name.downcase.to_sym == role_sym
  end

  def to_json(current_user)
    response = {
        id: id,
        name: name,
        email: email
    }
    response[:password] = password if current_user.has_role?(:admin)
    response
  end
end
