class Ticket < ApplicationRecord
  belongs_to :user

  def to_json
    {
        id: id,
        name: name,
        description: description,
        status: status,
        creator: user.name
    }
  end
end
