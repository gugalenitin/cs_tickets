class ApplicationController < ActionController::API
  include CanCan::ControllerAdditions

  def current_user
    session[:current_user_id] && User.find(session[:current_user_id])
  end
end
