class SessionsController < ApplicationController
  def create
    user = User.find_by(email: params[:username].downcase)
    if user && user.authenticate(params[:password])
      session[:current_user_id] = user.id
      render json: user.to_json(current_user)
    else
      render json: {error: 'Login failed!'}, status: 400
    end
  end

  def destroy
    session[:current_user_id] = nil
  end

  private

  def login_params
    params.permit(:username, :password)
  end
end
