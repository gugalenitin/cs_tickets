class TicketsController < ApplicationController
  load_and_authorize_resource

  rescue_from CanCan::AccessDenied, :with => :render_error

  def render_error
    render json: {error: 'User is not logged in or unauthorized to perform this action.'}, status: 401
  end

  def index
    tickets = Ticket.all
    tickets = tickets.where(user: current_user) if current_user.has_role? :customer
    render json: tickets.map { |t| t.to_json }
  end

  def create
    ticket = Ticket.new(ticket_params)
    ticket.status = 'NEW'
    ticket.user = current_user
    if ticket.user && ticket.save
      render json: ticket.to_json
    else
      render json: {error: 'Ticket creation failed!'}, status: 400
    end
  end

  def update
    ticket = Ticket.find(params[:ticket][:id])
    ticket.name = params[:ticket][:name]
    ticket.description = params[:ticket][:description]
    ticket.status = params[:ticket][:status]
    if ticket.save
      render json: ticket.to_json
    else
      render json: {error: 'Ticket updation failed!'}, status: 400
    end
  rescue ActiveRecord::RecordNotFound
    render json: {error: 'Ticket not found!'}, status: 400
  end

  private

  def ticket_params
    params.require(:ticket).permit(:name, :description)
  end
end
