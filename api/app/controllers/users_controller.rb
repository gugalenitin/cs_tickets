class UsersController < ApplicationController
  load_and_authorize_resource

  rescue_from CanCan::AccessDenied, :with => :render_error

  def render_error
    render json: {error: 'User is not logged in or unauthorized to perform this action.'}, status: 401
  end

  def index
    render json: User.all.map { |u| u.to_json(current_user) }
  end

  def create
    user = User.new(user_params)
    user.role = Role.find_by(name: params[:user][:role])
    if user.role && user.save
      render json: user.to_json(current_user)
    else
      render json: {error: 'User creation failed!'}, status: 400
    end
  rescue ActiveRecord::RecordNotUnique
    render json: {error: 'Email already exists!'}, status: 400
  end

  def update
    user = User.find(params[:user][:id])
    user.name = params[:user][:name]
    user.email = params[:user][:email]
    user.password = params[:user][:password] if params[:user][:password]
    user.role = Role.find_by(name: params[:user][:role])
    if user.role && user.save
      render json: user.to_json(current_user)
    else
      render json: {error: 'User updation failed!'}, status: 400
    end
  rescue ActiveRecord::RecordNotFound
    render json: {error: 'User not found!'}, status: 400
  rescue ActiveRecord::RecordNotUnique
    render json: {error: 'Email already exists!'}, status: 400
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password)
  end
end
