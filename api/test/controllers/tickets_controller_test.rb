require 'test_helper'

class TicketsControllerTest < ActionDispatch::IntegrationTest
  test 'should return all tickets to agent' do
    login('agent@crossover.com', 'agent123')

    get tickets_url
    assert_response :success
    assert_equal 2, JSON.parse(@response.body).length
  end

  test 'should return tickets owned by customer only' do
    login('customer@crossover.com', 'customer123')

    get tickets_url
    assert_response :success
    tickets = JSON.parse(@response.body)
    assert_equal 1, tickets.length
    assert_equal 'Test Ticket', tickets[0]['name']
  end

  test 'customer should be able to create/update ticket' do
    login('customer@crossover.com', 'customer123')

    post tickets_url, params: {ticket: {name: 'Test', description: 'Test Desc'}}
    assert_response :success

    ticket = JSON.parse(@response.body)
    assert_equal 'Test', ticket['name']
    assert_equal 'Test Desc', ticket['description']
    assert_equal 'NEW', ticket['status']
    assert_equal 'Customer User', ticket['creator']

    put "/api/tickets/#{ticket['id']}", params: {ticket: {id: ticket['id'], name: 'Test Updated', description: 'Test Desc', status: 'IN PROGRESS'}}
    assert_response :success

    ticket = JSON.parse(@response.body)
    assert_equal 'Test Updated', ticket['name']
    assert_equal 'IN PROGRESS', ticket['status']
  end

  test 'agent should not be able to create ticket' do
    login('agent@crossover.com', 'agent123')

    post tickets_url, params: {ticket: {name: 'Test', description: 'Test Desc'}}
    assert_response :unauthorized
  end

  test 'agent should be able to update ticket' do
    # Login as customer to create ticket
    login('customer@crossover.com', 'customer123')

    post tickets_url, params: {ticket: {name: 'Test', description: 'Test Desc'}}
    assert_response :success

    ticket = JSON.parse(@response.body)

    # Login as agent to update ticket
    login('agent@crossover.com', 'agent123')

    put "/api/tickets/#{ticket['id']}", params: {ticket: {id: ticket['id'], name: 'Test Updated', description: 'Test Desc', status: 'IN PROGRESS'}}
    assert_response :success

    ticket = JSON.parse(@response.body)
    assert_equal 'Test Updated', ticket['name']
    assert_equal 'IN PROGRESS', ticket['status']
  end
end
