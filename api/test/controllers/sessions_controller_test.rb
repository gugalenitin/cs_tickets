require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  test 'user should login successfully' do
    login('admin@crossover.com', 'admin123')

    assert_not_nil session[:current_user_id]
    assert_response :success
    assert_equal "application/json", @response.content_type
    user = JSON.parse(@response.body)
    assert_equal 'Admin User', user['name']
    assert_equal 'admin@crossover.com', user['email']
    assert_equal 'Admin', user['role']
  end

  test 'user should not login with invalid username/password' do
    post login_url, params: { username: 'admin@crossover.com', password: 'admin' }
    assert_nil session[:current_user_id]
    assert_response :bad_request
  end

  test 'user should logout successfully' do
    login('admin@crossover.com', 'admin123')
    assert_not_nil session[:current_user_id]

    # Logout
    delete logout_url
    assert_nil session[:current_user_id]
    assert_response :success
  end

end
