require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test 'should return all users to admin' do
    login('admin@crossover.com', 'admin123')

    get users_url
    assert_response :success
  end

  test 'should not permit access to users data for Customer' do
    login('customer@crossover.com', 'customer123')
    user_id = JSON.parse(@response.body)['id']

    get users_url
    assert_response :unauthorized
    post users_url, params: {user: {name: 'test'}}
    assert_response :unauthorized
    put "/api/users/#{user_id}"
    assert_response :unauthorized
  end

  test 'should not permit access to users data for Agent' do
    login('agent@crossover.com', 'agent123')
    user_id = JSON.parse(@response.body)['id']

    get users_url
    assert_response :unauthorized
    post users_url, params: {user: {name: 'test'}}
    assert_response :unauthorized
    put "/api/users/#{user_id}"
    assert_response :unauthorized
  end

  test 'admin should be able to create new user' do
    login('admin@crossover.com', 'admin123')

    post users_url, params: {user:{name: 'Test', email: 'test@crossover.com', password: 'test123', role: 'Customer'}}
    assert_response :success

    login('test@crossover.com', 'test123')
    assert_response :success
  end

  test 'admin should be able to update an existing user' do
    login('admin@crossover.com', 'admin123')

    post users_url, params: {user:{name: 'Test', email: 'test@crossover.com', password: 'test123', role: 'Customer'}}
    assert_response :success
    user_id = JSON.parse(@response.body)['id']

    put "/api/users/#{user_id}", params: {user:{id: user_id, name: 'Test Updated', email: 'testUpdated@crossover.com', password: 'test123', role: 'Agent'}}
    assert_response :success

    login('testUpdated@crossover.com', 'test123')
    assert_response :success
  end

  test 'should give bad_request for invalid input' do
    login('admin@crossover.com', 'admin123')

    post users_url, params: {user:{name: 'Test', email: 'test@crossover.com', password: 'test123', role: 'test'}}
    assert_response :bad_request
  end
end
