require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  test "role.name should not be null" do
    role = Role.new
    assert_raises(ActiveRecord::StatementInvalid) do
      role.save
    end
  end
end
