require 'test_helper'

class TicketTest < ActiveSupport::TestCase
  test "ticket.name, ticket.description, ticket.status should not be null" do
    ticket = Ticket.new
    assert_raises(ActiveRecord::StatementInvalid) do
      ticket.save
    end
  end
end
