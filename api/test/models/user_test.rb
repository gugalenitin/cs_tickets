require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "user.name, user.email, user.password_digest, user.role_id should not be null" do
    user = User.new
    assert_raises(ActiveRecord::RecordInvalid) do
      user.save!
    end
  end
end
